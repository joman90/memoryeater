﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;

namespace MemoryEaterLibrary
{
    public class MemoryEater
    {
        public delegate void OnEatingEndHandler();
        public static event OnEatingEndHandler EatingEnd;

        private List<MemoryStream> _managedBuffer;
        private List<IntPtr> _unmanagedBuffer;

        public void ManagedEat(long wasteGoal)
        {
            //store 100 Mb (100x1 Mb) of managed garbage
            _managedBuffer= new List<MemoryStream>(100);
            for (byte i = 0; i < 100; i++)
            {
                var temp = new MemoryStream(1024*1024);
                temp.Write(new byte[1024*1024], 0, 1024*1024);
                _managedBuffer.Add(temp);
            }

            if(Process.GetCurrentProcess().PrivateMemorySize64 < wasteGoal)
            {
                var tempThread = new Thread((() =>
                                             {
                                                var temp = new MemoryEater();
                                                temp.ManagedEat(wasteGoal);
                                             }));
                tempThread.Start();
                tempThread.Join();
            }
            else
            {
                Thread.Sleep(1000);
                if(EatingEnd == null) return;
                EatingEnd();
                EatingEnd = null;
            }
        }

        public void UnmanagedEat(long wasteGoal)
        {
            //reserve 100 Mb (100x1 Mb) of unmanaged memory
            _unmanagedBuffer = new List<IntPtr>(100);
            for (byte i = 0; i < 100; i++)
                _unmanagedBuffer.Add(Marshal.AllocHGlobal(1024*1024));

            if (Process.GetCurrentProcess().PrivateMemorySize64 < wasteGoal)
            {
                var tempThread = new Thread((() =>
                                             {
                                                 var temp = new MemoryEater();
                                                 temp.UnmanagedEat(wasteGoal);
                                             }));
                tempThread.Start();
                tempThread.Join();
            }
            else
            {
                Thread.Sleep(1000);
                if (EatingEnd == null) return;
                EatingEnd();
                EatingEnd = null;
            }
        }
    }
}
