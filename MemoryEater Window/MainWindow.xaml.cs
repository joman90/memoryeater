﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using MemoryEaterLibrary;

namespace MemoryEater_Window
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static CancellationTokenSource _cancel;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void CancelBT_Click(object sender, RoutedEventArgs e) { _cancel.Cancel(); }

        private async void StartBT_Click(object sender, RoutedEventArgs e)
        {
            StartBT.IsEnabled = false;
            CancelBT.IsEnabled = true;
            FrequencyTB.IsReadOnly = true;
            PortionTB.IsReadOnly = true;

            _cancel = new CancellationTokenSource();

            int freezeTime;
            int.TryParse(FrequencyTB.Text, out freezeTime);

            double portion;
            double.TryParse(PortionTB.Text, out portion);

            //convert portion size to bytes
            var wasteGoal = (long)Math.Round(portion*1024*1024*1024, MidpointRounding.AwayFromZero);

            if(managedRadioBT.IsEnabled)
            {
                do
                {
                    GC.AddMemoryPressure(wasteGoal); //prevent GC from collecting
                    await Task.Run(() => new MemoryEater().ManagedEat(wasteGoal));
                    GC.RemoveMemoryPressure(wasteGoal); //allow GC to collect again
                    GC.Collect();
                    await Task.Delay(freezeTime * 1000);
                } while (!_cancel.IsCancellationRequested);
            } else
            {
                do
                {
                    GC.AddMemoryPressure(wasteGoal); //prevent GC from collecting
                    await Task.Run(() => new MemoryEater().UnmanagedEat(wasteGoal));
                    GC.RemoveMemoryPressure(wasteGoal); //allow GC to collect again
                    GC.Collect();
                    await Task.Delay(freezeTime * 1000);
                } while (!_cancel.IsCancellationRequested);
            }

            StartBT.IsEnabled = true;
            CancelBT.IsEnabled = false;
            FrequencyTB.IsReadOnly = false;
            PortionTB.IsReadOnly = false;
        }
    }
}
